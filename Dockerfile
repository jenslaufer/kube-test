FROM nginx:stable-alpine

COPY nginx/index.html /www/data/index.html
COPY nginx/nginx.conf /etc/nginx/nginx.conf

CMD ["nginx", "-g", "daemon off;"]